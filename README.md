# voice_recognition

Aby uruchomić ropoznawanie pliku należy uruchomić main.py podając jako argument ścieżkę dostępu do nagrania, przyładowo

`main.py dir/002_M.wav`

Możliwe jest również uruchoienie rozoznawania plików w całym folderze. w tym wypadku jako argument podajemy ścieżkę do katalogu np.

`main.py dir`


Sprawozdanie z laboratorium:

Komunikacja człowiek-komputer

Projekt nr 2: Przetwarzanie dźwięku



1.  **Autorzy**


    Michał Płocki	132308	michalplocki2@gmail.com 
    
    Aleksander Stupnicki	132323	s.aleksander97@gmail.com
    
    Prowadząca: Agnieszka Mensfelt
    
    Zajęcia czwartek, 16:50

2.  **Cel projektu**

    Celem projektu było stworzenie programu, który na wejściowych plików .wav na bazie częstotliwości głosu rozpozna płeć osoby nagranej
    
2.  **Technologia**

    Projekt został wykonany w języku python, wersja 3. Do przetwarzania obrazów dźwięku zostały wykorzystane biblioteki scipy.signal i np. Do listowania plików w folderze użyto os.

3.  **Zasada Działania**

    1.Wybierana jest zasada działania( 0 -> pojedynczy plik, 1 -> wszystkie w folderze). W przypadku 1 tworzona jest pętla
    
    2.Za pomocą wavopenfile otrzymujemy dane oraz informacje o sample raitingu nagrania

    3.Przygotowujemy dane. Wymuszamy typ float, wycinamy odpowiedni(wcześniej ustalony) fragment nagrania
    
    4.Następnie normalizujemy dane za pomocą okna Hamminga, wykonujemy transformatę Fouriera i bierzemy z tego wartość bezwzględną
    
    5.Używając anty-alisasingowego filtru decimate, downsamplujemy sygnał ze współczynnikami 2, 3, 4 biorąc kolejno co drugą, trzecią i czwartą próbkę. Następnie wartości wszystkich wywołań funkcji wymnażamy przez siebie oraz przez niefiltrowany sygnał. Rezultat otrzymujemy przez wybranie maksymalnego argumentu tablicy z wynikiem naszego mnożenia dzieląc go przez rating
    
    6.Na koniec na podstawie wyliczonej częstotliwości sprawdzamy czy głos należy do mężczyzny czy do kobiety, porównując jej wartość z wartością progową 170 
    
    7.W przypadku wybrania opcji 1 na końcu następuje wypisanie jakości rozpoznania(poprzez porównanie rozpoznaj płci z nazwą pliku)


4.  **Wyniki**

    Z 91 testowych plików:
    
    * 85 Zidentyfikowanych poprawnie
    * 5 Zidentyfikowanych niepoprawnie
    * 1 niemożliwy do otwarcia
    
    Co daje skuteczność na poziomie 94,(4)%