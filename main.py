import numpy as np
import os
import scipy.io.wavfile as wavfile
import scipy.signal as signal
import warnings
import sys

def data_from_file(sound):
    sampling_rate, information = wavfile.read(sound)
    information = information.astype(float) / 2 ** 16
    if not isinstance(information[0], float):
        information = information[:, 0] + information[:, 1]
    sampling_rate = float(sampling_rate)
    return sampling_rate, information


def cut_data(uncut_data, divider):
    length = len(uncut_data)
    part = int(length / divider)
    uncut_data = uncut_data[part:length - part]
    return uncut_data


def decide_sex(freaquency):
    if freaquency > 170:
        voice_sex = 'K'
    else:
        voice_sex = 'M'
    return voice_sex


def prepare_files(arg):
    f_sounds = []
    f_sex = []
    for name in os.listdir(arg):
        if name.endswith(".wav"):
            f_sounds.append(name)
        if name.endswith("M.wav"):
            f_sex.append('M')
        if name.endswith("K.wav"):
            f_sex.append('K')
    return  f_sex, f_sounds



def hps(data, rate):
    processed = data * signal.hamming(len(data))
    processed = np.fft.rfft(processed)
    processed = abs(processed)
    decimated = []

    ds2 = signal.decimate(processed, 2)
    ds3 = signal.decimate(processed, 3)
    ds4 = signal.decimate(processed, 4)

    l = len(ds4)

    new = processed[:l] * ds2[:l] * ds3[:l] * ds4[:l]
    lcf = 70
    result = np.argmax(new[lcf:]) + lcf
    div = len(data) / rate

    return result / div


if __name__ == '__main__':
    arg = sys.argv[1]
    if arg.endswith('.wav'):
        try:
            warnings.filterwarnings('ignore')

            rate, data = data_from_file(arg)

            data = cut_data(data, 3)

            result = hps(data, rate)

            if arg.endswith("M.wav"):
                if decide_sex(result) == 'M':
                    print('Nazwa pliku:' + arg + ' Oczekiwany M: Uzyskany: ' + decide_sex(result) + ' Zgodnosc: Tak')
                else:
                    print('Nazwa pliku:' + arg + ' Oczekiwany M: Uzyskany: ' + decide_sex(result) + ' Zgodnosc: Nie')
            if arg.endswith("K.wav"):
                if decide_sex(result) == 'K':
                    print('Nazwa pliku:' + arg + ' Oczekiwany K: Uzyskany: ' + decide_sex(result) + ' Zgodnosc: Tak')
                else:
                    print('Nazwa pliku:' + arg + ' Oczekiwany K: Uzyskany: ' + decide_sex(result) + ' Zgodnosc: Nie')
        except:
            print('Blad otwierania')
    else:
        counter = 0
        excepts = 0
        sex, sounds = prepare_files(arg)

        for i in range(len(sounds)):
            try:
                warnings.filterwarnings('ignore')

                rate, data = data_from_file('dir/' + sounds[i])

                data = cut_data(data, 3)

                result = hps(data, rate)

                if sex[i] == decide_sex(result):
                    print('Nazwa pliku:' + sounds[i] + ' Oczekiwany:' + sex[i] + ' Uzyskany: ' + decide_sex(
                        result) + ' Zgodnosc: Tak')
                    counter += 1
                else:
                    print('Nazwa pliku:' + sounds[i] + ' Oczekiwany:' + sex[i] + ' Uzyskany: ' + decide_sex(
                        result) + ' Zgodnosc: Nie')

            except:
                excepts += 1
                print('Blad otwierania')

        print('Z probki: ' + str(len(sounds) - excepts) + ' plików poprawnie rozpoznano: ' + str(
            counter) + '\n% porpawnosci: ' + str(counter / (len(sounds) - excepts)))

